//
//  AbstractViewController.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 26/11/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit

class AbstractViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()

        loadGUI()
        loadData()
        bindGUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        layoutGUI()
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutGUI()
    }
    
    
    func loadGUI() {
        
    }
    
    
    func bindGUI() {
        
    }
    
    
    func layoutGUI() {
        
    }
    
    
    func loadData() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}
