//
//  MainDataSource.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 04/12/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit

class MainDataSource: NSObject {
    
    private enum numberCount: Int {
        case one = 1, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen, twenty
    }
    
    static let sharedDataSource = MainDataSource()
    private override init() {}
    
    private var plistData: [String:AnyObject] = [:]
    var imagesArray: NSArray?
    var baseURL: NSURL?
    
    
    func loadPlistData() {
        
        var format = PropertyListSerialization.PropertyListFormat.xml
        let plistPath:String? = Bundle.main.path(forResource: "application-settings", ofType: "plist")!
        let plistXML = FileManager.default.contents(atPath: plistPath!)!
        do {
            plistData = try PropertyListSerialization.propertyList(from: plistXML, options: .mutableContainersAndLeaves, format: &format) as! [String:AnyObject]
            
            print(plistData)
            
            if let urlString = plistData["baseURL"] as? String {
                baseURL = NSURL(string: urlString)
            }
            
            if let imagesURLs = plistData["imageURLs"] as? NSArray {
                imagesArray = imagesURLs
            }
        }
        catch {
            print("Error reading plist: \(error), format: \(format)")
        }
    }
    
    
    func loadNumbers() -> [String] {
        
        let randomNumbersArray = generateRandomNumbersArray()
        let describedNumbers = processStringNumbersFrom(array: randomNumbersArray)
        
        return describedNumbers!
    }
    
    
    private func generateRandomNumbersArray() -> [String] {
        
        var numbersArray = [String]()
        for _ in 0..<100 {
            numbersArray.append(String.random())
        }

        return numbersArray
    }
    
    
    private func processStringNumbersFrom(array: [String]?) -> [String]? {
        guard array != nil else {
            return nil
        }
        
        var describedNumbersArray = [String]()
        
        for number in array! {
            describedNumbersArray.append(process(string: number)!)
        }
        
        return describedNumbersArray
    }
    
    
    private func process(string: String?) -> String? {
        
        guard string != nil else {
            return nil
        }
        
        var baseString = string!
        var tempString = ""
        var mainString = ""
        
        for (index, char) in baseString.characters.enumerated() {
            tempString.append(char)
            if index < baseString.characters.count - 1 {
                if baseString.charAt(index) != baseString.charAt(index + 1) {
                    tempString.append(",")
                }
            }
        }
        
        let tempArray = tempString.components(separatedBy: ",")
        
        for (index, stringItem) in tempArray.enumerated() {
            
            mainString.append("\(numberCount.init(rawValue: stringItem.characters.count)!) \(stringItem.characters.first!)")
            if stringItem.characters.count > 1 {
                mainString.append("s")
            }
            if index != tempArray.count - 1 {
                mainString.append(", ")
            }
            
        }
        return mainString
    }
}


extension String {
    
    public func charAt(_ i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    public subscript (i: Int) -> String {
        return String(self.charAt(i) as Character)
    }
    
    public subscript (r: Range<Int>) -> String {
        return substring(with: self.characters.index(self.startIndex, offsetBy: r.lowerBound)..<self.characters.index(self.startIndex, offsetBy: r.upperBound))
    }
    
    public subscript (r: CountableClosedRange<Int>) -> String {
        return substring(with: self.characters.index(self.startIndex, offsetBy: r.lowerBound)..<self.characters.index(self.startIndex, offsetBy: r.upperBound))
    }
    
    static func random(length: Int = 20) -> String {
        let base = "0123456789"
        var randomString: String = ""
        
        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
