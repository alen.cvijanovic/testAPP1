//
//  LoadingViewController.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 11/12/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit

class LoadingViewController: AbstractViewController
{
    @IBOutlet var _activityView: UIActivityIndicatorView!
    @IBOutlet var _backgroundImageView: UIImageView!
    
    var loading: Bool = true
    {
        didSet
        {
            bindGUI()
        }
    }
    
    
    override func bindGUI()
    {
        
        if let animating = _activityView?.isAnimating
        {
            if loading && !animating
            {
                _activityView?.startAnimating()
            }
            else if !loading && animating
            {
                _activityView?.stopAnimating()
            }
        }
        
        
        if let imageName = launchImageNameForOrientation(orientation: UIApplication.shared.statusBarOrientation)
        {
            _backgroundImageView?.image = UIImage(named: imageName)
        }

    }
    
    
    func launchImageNameForOrientation(orientation: UIInterfaceOrientation) -> String? 
    {
        var screenSize = UIScreen.main.bounds.size
        var orientationString = "Portrait"
        
        if UIInterfaceOrientationIsLandscape(orientation)
        {
            screenSize = CGSize(width: screenSize.height, height: screenSize.width)
            orientationString = "Landscape"
        }
        
        if let infoDict = Bundle.main.infoDictionary,
            let imagesDict = infoDict["UILaunchImages"] as? [AnyObject]
        {
            for currentItem in imagesDict
            {
                if let currentItem = currentItem as? [String:AnyObject],
                    let currentSizeString = currentItem["UILaunchImageSize"] as? String,
                    let currentOrientationString = currentItem["UILaunchImageOrientation"] as? String
                {
                    if CGSizeFromString(currentSizeString).equalTo(screenSize) &&
                        orientationString == currentOrientationString
                    {
                        return currentItem["UILaunchImageName"] as? String
                    }
                }
            }
        }
        
        return nil
    }
}
