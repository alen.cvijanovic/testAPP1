//
//  FirstPageViewController.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 26/11/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD

protocol FirstPageDelegate: class {
    func switchToSecondChildVC()
}

class FirstPageViewController: AbstractViewController, UIWebViewDelegate{
    
    @IBOutlet private weak var webView: UIWebView!
    @IBOutlet private weak var imageView: UIImageView!
    
    @IBOutlet private weak var imageInfoView: UIView!
    @IBOutlet private weak var imageInfoDescriptionLabel: UILabel!
    @IBOutlet private weak var imageInfoTapImageView: UIImageView!
    
    private var imageViewLoading = false
    weak var delegate: FirstPageDelegate?
    
    override func loadGUI() {
        super.loadGUI()
        
        webView.delegate = self
        
        imageInfoView.isHidden = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tappedImageView(_:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isMovingToParentViewController == true {
            
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController == true {
            
        }
    }
    
    
    override func loadData() {
        super.loadData()
        
        if let url = MainDataSource.sharedDataSource.baseURL {
            webView.loadRequest(URLRequest(url: url as URL))
        }
        
    }
    
    override func bindGUI() {
        super.bindGUI()
        
        imageInfoDescriptionLabel.text = "Tap to load image"
    }
    
    
    override func layoutGUI() {
        super.layoutGUI()
        
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        weak var weakself = self
        if (request.url?.host?.contains("yahoo")) == true {
            weakself?.delegate?.switchToSecondChildVC()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            return false
        } else {
          return true
        }
    }
    
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        if let err = error as? URLError, err.code == .notConnectedToInternet {
            let alert = UIAlertController(title: "No Internet connection", message: "Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Error", message: "Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        print("error")
    }
    
    
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        if webView.canGoBack {
            webView.goBack()
        }
    }
    
    
    @IBAction func forwardButtonPressed(_ sender: UIBarButtonItem) {
        if webView.canGoForward {
            webView.goForward()
        }
    }
    
    
    @IBAction func homeButtonPressed(_ sender: UIBarButtonItem) {
        if let url = MainDataSource.sharedDataSource.baseURL {
            webView.loadRequest(URLRequest(url: url as URL))
        }
    }
    
    
    @IBAction func refreshButtonPressed(_ sender: UIBarButtonItem) {
        webView.reload()
    }
    
    
    func tappedImageView(_ sender: UITapGestureRecognizer) {
        
        guard imageViewLoading == false else {
            return
        }
        
        imageInfoView.isHidden = true
        
        // Choosing random image
        let images: NSArray = MainDataSource.sharedDataSource.imagesArray!
        let imageURL = images[Int(arc4random_uniform(10))] as! String
        
        weak var weakself = self
        let imageUrl = URL(string: imageURL)
        imageView.kf.indicatorType = .activity
        imageViewLoading = true
        
        //Images will be cached, but loading is forced every time
        imageView.kf.setImage(with: imageUrl, options: [.forceRefresh], completionHandler: {
            (image, error, cacheType, imageUrl) in
            weakself?.imageViewLoading = false
            if error == nil {
                print("imageLoadingFinished - success")
                
            } else {
                
                weakself?.imageInfoDescriptionLabel.text = "Image loading finished with error. \nPlease try again."
                weakself?.imageInfoView.isHidden = false
                print("imageLoadingFinished - error - \(error?.localizedDescription)")
                
            }
        })
    }
    
}
