//
//  MainViewController.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 26/11/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit
import MBProgressHUD

class MainViewController: AbstractViewController, FirstPageDelegate, SecondPageDelegate {
    
    private lazy var firstPageViewController: FirstPageViewController = {

        var viewController = FirstPageViewController()
        viewController.delegate = self
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    private lazy var secondPageViewController: SecondPageViewController = {

        var viewController = SecondPageViewController()
        viewController.delegate = self
        self.add(asChildViewController: viewController)
        
        return viewController
    }()

    
    override func loadGUI() {
        super.loadGUI()
        
        add(asChildViewController: firstPageViewController)
    }
    
    
    override func loadData() {
        super.loadData()
        
    }
    
    
    override func bindGUI() {
        super.bindGUI()
        
    }
    
    
    override func layoutGUI() {
        super.layoutGUI()
        
    }
    
    
    private func add(asChildViewController viewController: UIViewController) {
        
        addChildViewController(viewController)
        view.addSubview(viewController.view)
        
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        viewController.didMove(toParentViewController: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
       
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
    
    
    func switchToFirstChildVC() {
        weak var weakself = self
        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromLeft, animations:{
            weakself?.remove(asChildViewController: (weakself?.secondPageViewController)!)
            weakself?.add(asChildViewController: (weakself?.firstPageViewController)!)
        }, completion: nil)
    }
    
    
    func switchToSecondChildVC() {
        weak var weakself = self

        UIView.transition(with: self.view, duration: 0.5, options: UIViewAnimationOptions.transitionFlipFromRight, animations:{
            weakself?.remove(asChildViewController: (weakself?.firstPageViewController)!)
            weakself?.add(asChildViewController: (weakself?.secondPageViewController)!)
        }, completion: nil)
    }
}
