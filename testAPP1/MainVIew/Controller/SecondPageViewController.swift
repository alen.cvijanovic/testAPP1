//
//  SecondPageViewController.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 26/11/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol SecondPageDelegate: class {
    func switchToFirstChildVC()
}


class SecondPageViewController: AbstractViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.register(UINib(nibName: "SecondPageTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 80
            tableView.tableFooterView = UIView()
        }
    }
    
    private let cellIdentifier = "SecondPageCellIdentifier"
    var numbersArray = [String]()
    
    weak var delegate: SecondPageDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.layoutIfNeeded()
        
        MBProgressHUD.showAdded(to: view!, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        weak var weakself = self
        
        DispatchQueue.global().async {
            // generating random numbers and converting them to final format
            weakself?.numbersArray = MainDataSource.sharedDataSource.loadNumbers()
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: (weakself?.view)!, animated: true)
                weakself?.tableView.reloadData()
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        numbersArray = [String]()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numbersArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                 for: indexPath) as! SecondPageTableViewCell
        
        cell.titleText = numbersArray[indexPath.row]
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            delegate?.switchToFirstChildVC()
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.8,0.8,1)
            UIView.animate(withDuration: 0.1, animations: {
                cell.layer.transform = CATransform3DMakeScale(1,1,1)
            })
    }
}





