//
//  SecondPageTableViewCell.swift
//  testAPP1
//
//  Created by Alen Cvijanovic on 04/12/16.
//  Copyright © 2016 Alen Cvijanovic. All rights reserved.
//

import UIKit

class SecondPageTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    var titleText: String? { didSet { titleLabel.text = titleText } }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
